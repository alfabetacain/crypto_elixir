defmodule Set1Test do
  use ExUnit.Case

  test "Challenge 1 - Hex to base64" do
    hex_string = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
    base64 = Crypt.hex_to_base64!(hex_string)
    assert base64 == "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
  end

  test "Challenge 2 - Fixed XOR" do
    first_hex = "1c0111001f010100061a024b53535009181c"
    second_hex = "686974207468652062756c6c277320657965"
    res = Crypt.fixed_xor_with_hex(first_hex, second_hex) |> Crypt.binary_to_hex()
    expected_hex = "746865206b696420646f6e277420706c6179"
    assert res == expected_hex
  end

  test "Challenge 3 - Single-byte XOR cipher" do
    hex_string = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
    {key, _score} = Crypt.crack_single(hex_string)
    decrypted = Crypt.decrypt_single_byte(Crypt.hex_to_binary(hex_string), key)
    assert decrypted == "Cooking MC's like a pound of bacon"
  end

  test "Challenge 4 - Detect single-character XOR" do
    file = Application.get_env(:crypto, :set_1_challenge_4_text)
    stream = IO.stream(File.open!(file), :line)
    hexes = stream |> Enum.into([]) |> Enum.map(&(String.strip/1))
    {hex, {key, _score}} = Crypt.crack_single(hexes)
    decrypted = Crypt.decrypt_single_byte(Crypt.hex_to_binary(hex), key)
    assert decrypted == "Now that the party is jumping\n"
  end

  test "Challenge 5 - Implement repeating-key XOR" do
    text = "Burning 'em, if you ain't quick and nimble
I go crazy when I hear a cymbal"
    res = Crypt.binary_to_hex(Crypt.fixed_xor(text, "ICE"))
    expected = "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f"
    assert res == expected
  end

  test "Hamming distance works" do
    first = "this is a test"
    second = "wokka wokka!!!"
    distance = Crypt.hamming_distance(first, second)
    assert distance == 37
  end
  
  test "Challenge 6 - Break repeating-key XOR" do
    file = Application.get_env(:crypto, :set_1_challenge_6_text)
    binary = IO.binread(File.open!(file), :all)
    #IO.puts binary
    {:ok, decoded} = Base.decode64(binary, ignore: :whitespace)

    [key | _] = Crypt.break_repeating_key_xor(decoded, 2..41)
    expected = IO.read(File.open!(Application.get_env(:crypto, :set_1_challenge_6_solution)), :all)
    res = Crypt.fixed_xor(decoded, key)
    assert expected == res
  end

  test "Challenge 7 - AES in ECB mode" do
    key = "YELLOW SUBMARINE"
    binary = File.open!(Application.get_env(:crypto, :set_1_challenge_7_text)) |> IO.binread(:all) |> Base.decode64!(ignore: :whitespace)
    decrypted = Crypt.aes_ecb_decrypt(key, binary)
    expected = IO.read(File.open!(Application.get_env(:crypto, :set_1_challenge_7_solution)), :all) |> String.rstrip(?\n)
    assert expected == decrypted
  end

  test "Challenge 8 - Detect AES in ECB mode" do
    binary = File.open!(Application.get_env(:crypto, :set_1_challenge_8_text)) |> IO.binread(:all)
    lines = String.split(binary, "\n", trim: true) |> Enum.map(&String.strip(String.strip(&1, ?\r), ?\n))
    results = lines |> Enum.map(&Crypt.hex_to_binary/1) |> Enum.map(fn bin -> {Crypt.break_repeating_key_xor(bin, 2..20), bin} end)
    best = results 
    |> Enum.map(fn {key, bin} -> Crypt.fixed_xor(bin, key) end)
    |> Enum.map(fn plain -> {plain, Crypt.score_english(plain)} end)
    |> Enum.max_by(fn {_, score} -> score end)

    {text, score} = best
    IO.puts score
    IO.puts text
    assert 1 == 2
  end
end
