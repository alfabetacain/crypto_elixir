defmodule Crypt do
  require Bitwise
  require Logger

  def pmap(collection, fun) do
    me = self
    collection
    |> Enum.map(fn (elem) -> 
      spawn_link fn -> (send me, { self, fun.(elem)}) end
      end)
    |> Enum.map(fn pid ->
        receive do { ^pid, result } -> result end
      end)
  end

  def hex_to_base64!(string) when is_binary(string) do
    Base.decode16!(string, case: :lower) |> Base.encode64
  end

  def hex_to_binary(string) when is_binary(string) do
    Base.decode16!(string, case: :lower)
  end

  def binary_to_hex(binary) when is_binary(binary), do: Base.encode16(binary, case: :lower)

  def fixed_xor_with_hex(first, second) do
    _fixed_xor(hex_to_binary(first), hex_to_binary(second))
  end

  def _fixed_xor(<<>>, _), do: <<>>
  def _fixed_xor(<<f :: size(8), frest :: binary>>, <<s :: size(8), srest :: binary>>) do
    <<Bitwise.bxor(f,s)>> <> _fixed_xor(frest,srest)
  end

  def fixed_xor(bin, xorer) when is_binary(bin) do
    if byte_size(bin) > byte_size(xorer) do
      big_xorer = 0..div(byte_size(bin),byte_size(xorer)) |> Enum.into([]) |> Enum.map(fn _ -> xorer end)
        |> Enum.reduce(<<>>, fn elem, acc -> elem <> acc end)
      _fixed_xor(bin, big_xorer)
    else 
      _fixed_xor(bin, xorer)
    end
  end

  def hamming_distance_byte(
    <<f1 :: size(1),f2 :: size(1),f3 :: size(1),f4 :: size(1),f5 :: size(1),f6 :: size(1),f7 :: size(1),f8 :: size(1)>>,
    <<s1 :: size(1),s2 :: size(1),s3 :: size(1),s4 :: size(1),s5 :: size(1),s6 :: size(1),s7 :: size(1),s8 :: size(1)>>) do
      Enum.zip([f1, f2, f3, f4, f5, f6, f7, f8], [s1, s2, s3, s4, s5, s6, s7, s8])
      |> Enum.reduce(0, fn {f, s}, acc -> 
        if f == s do acc 
        else 1 + acc end 
      end)
  end

  def do_hamming_distance(<<>>, <<>>, acc), do: acc
  def do_hamming_distance(<<first :: size(8), frest :: binary>>, <<second :: size(8), srest :: binary>>, acc) do
    :binary.bin_to_list()
    do_hamming_distance(frest, srest, hamming_distance_byte(<<first>>, <<second>>) + acc)
  end

  def hamming_distance(first, second) do
    Enum.zip(binary_to_list_of_bytes(first), binary_to_list_of_bytes(second))
    |> Enum.reduce(0, fn {f, s}, acc -> hamming_distance_byte(f, s) + acc end)
    #do_hamming_distance(first, second, 0)
  end

  def binary_to_list_of_bytes(<<>>), do: []
  def binary_to_list_of_bytes(<<x :: size(8), rest :: binary>>), do: [<<x>> | binary_to_list_of_bytes(rest)]

  def score_english(string) do
    count_english_chars(string)
  end

  def count_english_chars(<<>>), do: 0
  def count_english_chars(<<char :: size(8), rest :: binary>>) do
    if is_english_char(char) do
      1 + count_english_chars(rest)
    else
      count_english_chars(rest)
    end
  end

  def is_english_char(char) do
    char > 64 and char < 91 or char > 96 and char < 123 or char == ?\s or char == ?' or char == ?.
  end

  def decrypt_single_byte(cipher_text, key) when is_binary(key) do
    fixed_xor(cipher_text, key)
  end

  def crack_single_bytes(bytes) do
    ls = Enum.map(0..255, fn x -> <<x :: size(8)>> end)
    mapped = Enum.map(ls, fn key -> {key, fixed_xor(bytes, key)} end)#&(fixed_xor(bytes, &1)))
    scored = Enum.map(mapped, fn {key, s} -> {key, score_english(s)} end)
    scored |> Enum.reverse |> Enum.max_by(fn {_key, score} -> score end)
  end

  def crack_single(hexes) when is_list(hexes) do
    results = pmap(hexes, fn hex -> {hex, crack_single(hex)} end)#&crack_single/1)
    Enum.max_by(results, fn {_, {_, score}} -> score end)
  end

  def crack_single(hex_string) do
    decoded = Base.decode16!(hex_string, case: :lower)
    crack_single_bytes(decoded)
  end

  def list_of_bytes_to_binary(list) do
    Enum.reduce(list, <<>>, fn byte, acc -> acc <> byte end)
  end

  def get_hamming_distance_with_keysize(cipher_text, keysize) do
    list_of_bytes = binary_to_list_of_bytes(cipher_text)
    chunks = Enum.chunk(list_of_bytes, keysize*2)
    sum = Enum.map(chunks, fn both ->
      {part1, part2} = Enum.split(both, keysize)
      hamming_distance(list_of_bytes_to_binary(part1), list_of_bytes_to_binary(part2)) / keysize
    end)
    |> Enum.sum
    sum / (Enum.count(list_of_bytes) / keysize - 1)
    #first_part = Enum.slice(list_of_bytes, 0, keysize) |> Enum.reduce(<<>>, &(&2 <> &1))
    #second_part = Enum.slice(list_of_bytes, keysize, keysize) |> Enum.reduce(<<>>, &(&2 <> &1))
    #hamming_distance(first_part, second_part) / keysize
  end

  def break_repeating_key_with_size(cipher_text, keysize) do
    blocks = Enum.chunk(binary_to_list_of_bytes(cipher_text), keysize)
    #Logger.info "Number of blocks: #{Enum.count(blocks, fn _ -> true end)}"
    Enum.map(0..(keysize-1), fn size ->
      Enum.reduce(blocks, <<>>, fn ls, acc -> 
        <<next_byte :: size(8)>> = List.first(Enum.slice(ls, size, 1))
        acc <> <<next_byte>>
      end)
    end)
    |> Enum.map(fn bytes ->
        crack_single_bytes(bytes) end)
    |> Enum.map(fn {key, _score} -> 
      key end)
    |> Enum.reduce(<<>>, fn key, acc -> acc <> key end)
  end

  def break_repeating_key_xor(cipher_text, range) do
    best_keysizes = Enum.map(range, &({&1, get_hamming_distance_with_keysize(cipher_text, &1)}))
    |> Enum.sort_by(fn {_, prob} -> prob end)
    |> Enum.take(1)
    #Logger.info inspect best_keysizes
    best_keysizes 
    |> Enum.map(fn {key, _prob} -> key end)
    |> Enum.map(&(break_repeating_key_with_size(cipher_text, &1)))
  end

  def aes_ecb_decrypt(key, cipher_text) do
    :crypto.block_decrypt(:aes_ecb, key, cipher_text)
  end

end
